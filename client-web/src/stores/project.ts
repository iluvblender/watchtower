import { reactive } from 'vue';
import axios from 'axios';
import dataUrls from '@/lib/dataurls';
import colors from '@/lib/colors';
import type { Asset, AssetType, TaskType, TaskStatus, ProcessedUser, Sequence, Shot, ShotCasting, VideoPlayerSource } from '@/types.d.ts';

const basePath = import.meta.env.BASE_URL;

export class DataProjectStore {
  id = '';
  name = '';
  ratio = 0;
  resolution = '';
  assetTypes: AssetType[] = [];
  taskTypes: TaskType[] = [];
  taskStatuses: TaskStatus[] = [];
  team: ProcessedUser[] = [];
  thumbnailUrl = '';
  sequences: Sequence[] = [];
  shots: Shot[] = [];
  assets: Asset[] = [];
  totalFrames = 1;
  frameOffset = 0;
  fps = 24;
  videoPlayerSources: VideoPlayerSource[] = [];
}


export class useProjectStore {
  data = reactive(new DataProjectStore());

  async fetchProjectData(projectId: string) {
    try {

      const response = await axios.get(dataUrls.getUrl(dataUrls.urlType.Project, projectId));
      this.data.id = response.data.id;
      this.data.name = response.data.name;
      this.data.ratio = response.data.ratio;
      this.data.resolution = response.data.resolution;
      this.data.thumbnailUrl = response.data.thumbnailUrl;
      this.data.fps = response.data.fps;

      colors.batchAssignColor(response.data.asset_types);
      this.data.assetTypes = response.data.asset_types;
      colors.batchConvertColorHexToRGB(response.data.task_types);
      this.data.taskTypes = response.data.task_types;
      colors.batchConvertColorHexToRGB(response.data.task_statuses);
      this.data.taskStatuses = response.data.task_statuses;

      // Reference all users from the context
      const projectTeam = response.data.team;
      // If the project has any user referenced (as list of IDs)
      if (projectTeam.length > 0) {
        const processedUsers = [];
        for (const filteredUser of projectTeam) {
          // Build a processed user
          // TODO: Use the existing User type, not ProcessedUser
          const user: ProcessedUser = {
            name: filteredUser.full_name,
            id: filteredUser.id,
            profilePicture: `${basePath}static/img/placeholder-user.png`,
            color: undefined
          }
          if (filteredUser.has_avatar) {
            user.profilePicture = `${basePath}${filteredUser.thumbnailUrl}`;
          }
          processedUsers.push(user);
        }
        colors.batchAssignColor(processedUsers);
        this.data.team = processedUsers;
      }

    } catch (error) {
      console.log(error)
    }
  }
  async fetchProjectSequences(projectId: string) {
    try {
      const response = await axios.get(dataUrls.getUrl(dataUrls.urlType.Sequences, projectId));
      // Setup data for Sequences.
      for (let i = 0; i < response.data.length; i++) {
        const seq = response.data[i];
        seq.color = colors.paletteDefault[i];
      }
      this.data.sequences = response.data;

      // Perform asset casting
      // for (let i = 0; i < this.data.sequences.length; i++) {
      //   const seq = response.data[i];
      //   await this.data.fetchSequenceCasting(projectId, seq.id)
      // }
    } catch (error) {
      console.log(error)
    }
  }
  async fetchProjectShots(projectId: string) {
    try {
      const response = await axios.get(dataUrls.getUrl(dataUrls.urlType.Shots, projectId));
      for (const shot of response.data) {
        if (shot.thumbnailUrl === null) {
          shot.thumbnailUrl = `${basePath}static/img/placeholder-asset.png`
        } else {
          // Prepend the base_url
          const basePath = import.meta.env.BASE_URL;
          shot.thumbnailUrl = `${basePath}${shot.thumbnailUrl}`
        }
        shot.asset_ids = [];
      }
      this.data.shots = response.data;
      this.data.shots.sort((a, b) => (a.startFrame > b.startFrame) ? 1 : -1)
    } catch (error) {
      console.log(error)
    }
  }
  async fetchProjectCasting(projectId: string) {
    try {
      const response = await axios.get(dataUrls.getUrl(dataUrls.urlType.Casting, projectId));
      for (const shot of this.data.shots) {
        // We assume that shotCasting.shot_id is unique
        const filteredCasting = response.data.find((shotCasting: ShotCasting) => shotCasting.shot_id === shot.id);
        if (filteredCasting === undefined) { continue }
        shot.asset_ids = filteredCasting.asset_ids;
        // shot.asset_ids = [];
      }

      for (const asset of this.data.assets) {
        asset.shot_ids = response.data
          .filter((s: ShotCasting) => s.asset_ids.includes(asset.id))
          .map((s: ShotCasting) => s.shot_id);
      }

    } catch (error) {
      console.log(error);
    }
  }
  async fetchEditData(projectId: string) {
    const urlEdit = `${basePath}data/projects/${projectId}/edit.json`
    const response = await axios.get(urlEdit);
    this.data.totalFrames = response.data.totalFrames;
    this.data.frameOffset = response.data.frameOffset;
    this.data.videoPlayerSources = [
      {
        src: `${basePath}${response.data.sourceName}`,
        type: response.data.sourceType,
      }
    ]
    // this.data.setCurrentFrame(response.data.frameOffset);

  }
  async fetchProjectAssets(projectId: string) {
    try {
      const response = await axios.get(dataUrls.getUrl(dataUrls.urlType.Assets, projectId));
      for (const asset of response.data) {
        if (asset.thumbnailUrl === null) {
          asset.thumbnailUrl = `${basePath}static/img/placeholder-asset.png`
        } else {
          // Prepend the base_url
          const basePath = import.meta.env.BASE_URL;
          asset.thumbnailUrl = `${basePath}${asset.thumbnailUrl}`
        }
        // This will be populated via fetchSequenceCasting()
        asset.shot_ids = [];
      }
      this.data.assets = response.data;
    } catch (error) {
      console.log(error);
    }
  }
  async initWithProject(projectId: string) {
    try {
      await this.fetchProjectData(projectId);
      await this.fetchProjectShots(projectId);
      await this.fetchProjectAssets(projectId);
      await this.fetchProjectSequences(projectId);
      await this.fetchProjectCasting(projectId)
      await this.fetchEditData(projectId);
    } catch (error) {
      console.log(error)
    }
  }
}
